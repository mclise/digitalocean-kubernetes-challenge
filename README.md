# DigitalOcean Kubernetes Challenge


## Deploying a log monitoring system (Datadog) in DigitalOcean managed Kubernetes

I decided to use Datadog for this challenge as I already have some experience running it in a Docker container at work. Datadog is a closed-source monitoring product, and an account is required to utilize it. There is a free trial available if you’re interested in following along.

To generate logs, we’re going to use an image on DockerHub, chentex/random-logger.

To get started, go to your DigitalOcean account to provision a new Kubernetes cluster. Follow the instructions provided to connect to your new Kubernetes context using the official Kubernetes command-line tool, kubectl.

Next we install helm and make sure that it's using the context that we just created. Instructions for this installation can be found on the [helm website](https://helm.sh).

Next we'll create a trial Datadog account and follow the on screen instructions to install the Datadog Agent on our cluster using helm.

![datadog instructions](https://mclise.sfo3.digitaloceanspaces.com/Screenshot%20from%202021-12-31%2013-37-01.png)

During this process, you'll need to create a configuration file that will overwrite the default config of the agent. There are a couple relevant settings that will need to be changed in order for the agent to begin collection logs. Make sure the following are set in your overwrites:

```
datadog:
  logs:
    enabled: true
    containerCollectAll: true
```

After following these instructions, we can see our first logs coming from our cluster. We can see that the logs being generated are from the node in our cluster.

![host node name](https://mclise.sfo3.digitaloceanspaces.com/Screenshot%20from%202021-12-31%2014-36-52.png)

![first logs](https://mclise.sfo3.digitaloceanspaces.com/Screenshot%20from%202021-12-31%2013-43-14.png)

Next, we create the deployment configuration for our random-logger deployment, random-logger.yaml. We create the deployment using kubectl. In this instance we'll be creating two replicas of the random-logger image.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: random-logger-deployment
  labels:
    app: random-logger
spec:
  replicas: 2
  selector:
    matchLabels:
      app: random-logger
  template:
    metadata:
      labels:
        app: random-logger
    spec:
      containers:
      - name: random-logger
        image: chentex/random-logger
        ports:
        - containerPort: 80
```

After creating the deployment via kubectl, we can verify that the logs from random-logger are being created in the pods and collected by the datadog agent.

![random-logger logs](https://mclise.sfo3.digitaloceanspaces.com/Screenshot%20from%202021-12-31%2013-51-48.png)

We can check our Kubernetes Dashboard in our DO account to see that everything is running properly.

![k8s dashboard](https://mclise.sfo3.digitaloceanspaces.com/Screenshot%20from%202021-12-31%2013-57-34.png)

This set up should auto detect any other containers you start and collect their logs with no further configuration!






### Steps for project:
1. Set up Kubernetes context on Digital Ocean
2. Install Helm
3. Create Datadog account (free trial)
4. Install Datadog agent on cluster using helm
5. Create datadog-values.yaml
6. Overwrite logging default values
7. Create deployment config for random-logger
8. Confirm that logs are being generated, collected, and are viewable on the Datadog dashboard
